package bigdata.scala.spark.streaming.task.testscalaapp

import bigdata.java.framework.spark.kafka.{SKProcessor, SimpleSparkKafka}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.streaming.api.java.JavaInputDStream

object ScalaDemo {
  def main(args: Array[String]): Unit = {
    val simpleSparkKafka = new SimpleSparkKafka(args)
    simpleSparkKafka.execute(new SKProcessor {
      override def process(directStream: JavaInputDStream[ConsumerRecord[String, String]]): Unit = {
        val stream = directStream.inputDStream
        stream.foreachRDD{rdd=>
          rdd.foreachPartition { partitionOfRecods =>
            partitionOfRecods.foreach(println)
          }
        }
      }
    }).start()
  }
}
