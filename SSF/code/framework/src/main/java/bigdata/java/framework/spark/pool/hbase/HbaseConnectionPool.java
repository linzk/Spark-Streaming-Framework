/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.hbase;

import bigdata.java.framework.spark.pool.ConnectionPool;
import bigdata.java.framework.spark.pool.ConnectionPoolBase;
import org.apache.hadoop.hbase.client.Connection;

public class HbaseConnectionPool extends ConnectionPoolBase<Connection> implements ConnectionPool<Connection> {

    public HbaseConnectionPool(HbaseConfig hbaseConfig) {
        super(hbaseConfig, new HbaseConnectionFactory(hbaseConfig));
    }

    @Override
    public Connection getConnection() {
        return super.getResource();
    }

    @Override
    public void returnConnection(Connection conn) {
        super.returnResource(conn);
    }

    @Override
    public void invalidateConnection(Connection conn) {
        super.invalidateResource(conn);
    }
}
