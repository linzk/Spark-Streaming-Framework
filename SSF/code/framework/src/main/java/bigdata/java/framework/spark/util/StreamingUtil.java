/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.TaskContext;
import org.apache.spark.api.java.JavaRDDLike;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.api.java.AbstractJavaDStreamLike;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.scheduler.*;
import redis.clients.jedis.Jedis;
import scala.collection.Iterator;

import java.util.*;
import java.util.stream.Stream;

/**
 * spark streaming 工具类
 */
public class StreamingUtil {

    /**
     * spark 日志级别WARN
     */
    public static final String LOGLEVEL = "WARN";


    /**
     * 获取application.properties相关spark配置项
     * @param properties 指定配置文件名称
     */
    public static SparkConf loadGeneralConf(String properties)
    {
        Config config = ConfigFactory.load(properties);
        SparkConf sparkConf = new SparkConf();
        Stream<Map.Entry<String, ConfigValue>> filterConfig = config.entrySet().stream()
                .filter(e -> e.getKey().startsWith("sparkconf."));
        filterConfig.forEach(e -> sparkConf.set(
                e.getKey().replace("sparkconf.", "")
                , config.getString(e.getKey())));

        return sparkConf;
    }

    /**
     * 本地模式设置一些特殊配置。
     */
    public static void localModeConfig()
    {
        System.setProperty("hadoop.home.dir", "D:\\hadoop-common-2.2.0-bin-master");
        System.setProperty("HADOOP_USER_NAME", "yxgk");
    }

    /**
     * 创建spark streaming配置
     */
    public static SparkConf createGeneralConf(String appName)
    {
        return createGeneralConf(appName,"application.properties");
    }
    /**
     * 创建spark streaming配置
     */
    public static SparkConf createGeneralConf(String appName, String properties)
    {
        SparkConf conf = loadGeneralConf(properties);
//        if(StringUtils.isNotEmpty(master)){
//            conf.setMaster(master);
//        }
        conf.setAppName(appName);
        if(conf.get("spark.master")!=null)
        {//本地模式，增加一些特殊配置
            localModeConfig();
        }
        return conf;
    }

    /**
     * 触发output操作
     * @param dStreamLike JavaDStream或者JavaPairDStream
     */
    public static void output(AbstractJavaDStreamLike dStreamLike)
    {
        dStreamLike.foreachRDD(new VoidFunction() {
            @Override
            public void call(Object o) throws Exception {
                JavaRDDLike javaRDD = (JavaRDDLike)o;
                javaRDD.count();
            }
        });
    }

    /**
     *foreachPartition task info
     */
    public static String getTaskExecInfo()
    {
        TaskContext taskContext = TaskContext.get();
        int partitionId = taskContext.partitionId();
//        int attemptNumber = taskContext.attemptNumber();
//        int stageAttemptNumber = taskContext.stageAttemptNumber();
        int stageId = taskContext.stageId();
        long taskAttemptId = taskContext.taskAttemptId();
        String batchTime = taskContext.getLocalProperty("spark.streaming.internal.batchTime");
        String bTime = DateUtil.stampToDate(Long.valueOf(batchTime), "yyyy/MM/dd HH:mm:ss");
        String outputOpId = taskContext.getLocalProperty("spark.streaming.internal.outputOpId");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{\"BatchTime\":\"")
                .append(bTime)
                .append("\",\"OutputOpId\":")
                .append(outputOpId)
                .append(",\"StagId\":")
                .append(stageId)
                .append(",\"Tasks.Index\":")
                .append(partitionId)
                .append(",\"Tasks.ID\":")
                .append(taskAttemptId)
                .append("}");
        return stringBuilder.toString();
    }

    /**
     * redis优雅关闭spark streaming
     * @param appName 应用程序名称
     * @param javaStreamingContext JavaStreamingContext
     * @throws InterruptedException InterruptedException
     */
    private static void stopByMarkKeyRedis(String appName, JavaStreamingContext javaStreamingContext) throws InterruptedException {
        int intervalMills = 10 * 1000; // 每隔10秒扫描一次消息是否存在
        Long stopTime = 60000L;//默认停止时间
        boolean isStop = false;
        boolean beginKill = false;//true表示开始停止程序了
        while (!isStop) {
            if(beginKill){
                return;
            }
            isStop = javaStreamingContext.awaitTerminationOrTimeout(intervalMills);
          //  System.out.println(isStop);
            Jedis jedis = JedisUtil.getInstance().getJedis();
            String value = jedis.get(appName);
            String flag = "";
            if(value == null|| StringUtils.isEmpty(value))
            {
                jedis.set(appName,"1:"+stopTime);
                flag = "1";
            }else{
                flag = value.split(":")[0];
            }
            if (!isStop && flag.equals("0")) {

                stopTime = Long.valueOf(jedis.get(appName).split(":")[1]);
                //删除key
                beginKill = true;
                jedis.del(appName);
                System.out.println(stopTime+" second after close "+appName+" app.....");
                Thread.sleep(stopTime);
                javaStreamingContext.stop(true, true);

            }
            jedis.close();
        }
    }

    /**
     * 开启spark streaming kafka反压机制
     * @param sparkConf SparkConf
     * @param number 每秒条数
     */
    public static SparkConf setBackPressure(SparkConf sparkConf , Integer number)
    {
        sparkConf.set("spark.streaming.backpressure.enabled","true");
        sparkConf.set("spark.streaming.receiver.maxRate",String.valueOf(number));
        sparkConf.set("spark.streaming.kafka.maxRatePerPartition",String.valueOf(number));
        return sparkConf;
    }

    /**
     * 开启spark streaming 优雅关闭,类型支持：Mysql，Hbase，Redis；默认值：Hbase
     * @param appName  应用程序名称，必须唯一
     * @param jssc JavaStreamingContext JavaStreamingContext
     * @throws InterruptedException InterruptedException
     */
    public static void stopByMark(String appName,JavaStreamingContext jssc) throws InterruptedException {
        Config config = ConfigUtil.config;
        if(!config.getIsNull("StopByMark"))
        {
            String stopByMark = config.getString("StopByMark");
            if("MySql".equals(stopByMark))
            {//mysql
                stopByMarkKeyMySql(appName,jssc);
            }
            else if("Hbase".equals(stopByMark))
            {//hbase
                stopByMarkKeyHbase(appName,jssc);
            }
            else if("Redis".equals(stopByMark))
            {//redis
                stopByMarkKeyRedis(appName,jssc);
            }
            else
            {//hbase
                stopByMarkKeyHbase(appName,jssc);
            }
        }
        else
        {//hbase
            stopByMarkKeyHbase(appName,jssc);
        }
    }

    /**
     * hbase优雅关闭spark streaming
     * @param appName 应用程序名称
     * @param javaStreamingContext JavaStreamingContext
     * @throws InterruptedException InterruptedException
     */
    public static void stopByMarkKeyHbase_bak(String appName, JavaStreamingContext javaStreamingContext) throws InterruptedException {
        //运行程序创建表
        String tableName = "SPARKSTREAMING:STOPBYMARK";
        HBaseUtil.createTableNotExists(tableName,"F");
        Long stopTime = Long.valueOf (30 * 1000);//默认停止时间
        int intervalMills = 10 * 1000; // 每隔10秒扫描一次消息是否存在
        boolean isStop = false;
        boolean beginKill = false;//true表示开始停止程序了
        while (!isStop) {
            isStop = javaStreamingContext.awaitTerminationOrTimeout(intervalMills);
            if(isStop)
            {
                System.out.println("程序"+appName+"已经退出exit！");
                System.exit(0);
            }
            if(beginKill){
                return;
            }
            //获取标识
            String flag = HBaseUtil.get(tableName, appName, "F", "FLAG");
            if(flag == null|| StringUtils.isEmpty(flag))
            {
                Date date = new Date();
                Map<String, String > map = new HashMap<>();
                map.put("FLAG","1");
                map.put("STOPTIME",stopTime.toString());
                map.put("UPTIME",fastDateFormat.format(date));
                HBaseUtil.put(tableName, appName, "F", map);
                flag = "1";
            }
            if (!isStop && flag.equals("0")) {
                //删除key
                stopTime = Long.valueOf(HBaseUtil.get(tableName, appName, "F", "STOPTIME")) ;
                beginKill = true;
                //停止程序，删除停止标志
                HBaseUtil.deleteRow(tableName, appName);
                System.out.println(stopTime + " millis sencond  after close "+appName+" app.....");
                Thread.sleep(stopTime);
                javaStreamingContext.stop(true, true);
            }
        }
    }

    /**
     * hbase优雅关闭spark streaming
     * @param appName 应用程序名称
     * @param javaStreamingContext JavaStreamingContext
     * @throws InterruptedException InterruptedException
     */
    public static void stopByMarkKeyHbase(String appName, JavaStreamingContext javaStreamingContext) throws InterruptedException {
        //运行程序创建表
        String tableName = "SPARKSTREAMING:STOPBYMARK";
        HBaseUtil.createTableNotExists(tableName,"F");
        Long stopTime = Long.valueOf (30 * 1000);//默认停止时间
        int intervalMills = 10 * 1000; // 每隔10秒扫描一次消息是否存在
        boolean isStop = false;
        boolean beginKill = false;//true表示开始停止程序了
        while (!isStop) {
            synchronized (lock) {
                isStop = javaStreamingContext.awaitTerminationOrTimeout(intervalMills);
                if (isStop) {
                    HBaseUtil.deleteRow("SPARKSTREAMING:STOPBYMARK",appName);
                    String upStatus = "update Task set `status`=10 where APPNAME='" + appName+"'";
                    MySqlUtil.getInstance().ExecuteSQL(upStatus);
                    System.out.println("程序 "+appName+" 已经停止stop！");
                }

                if (beginKill) {
                    return;
                }
                //获取标识
                String flag = "";
                Map<String, String> maps = HBaseUtil.get(tableName, appName, "F", new String[]{"FLAG", "STOPTIME"});
                if(maps.size() > 0)
                {
                    flag = maps.get("FLAG");
                    stopTime = Long.valueOf(maps.get("STOPTIME").toString());
                }

                if (!isStop && flag.equals("0")) {//flag 为0表示需要停止程序
                    //删除key
                    beginKill = true;
                    System.out.println("stop by mark close "+appName+" app.....");
                    javaStreamingContext.stop(true, true);
                }

                if(StringUtils.isEmpty(flag) && !beginKill)
                {
                    Date date = new Date();
                    Map<String, String> columnAndValues = new HashMap<>();
                    columnAndValues.put("FLAG","1");
                    columnAndValues.put("STOPTIME",String.valueOf(stopTime));
                    columnAndValues.put("UPTIME",fastDateFormat.format(date));
                    HBaseUtil.put("SPARKSTREAMING:STOPBYMARK",appName,"F",columnAndValues);
                }
                Thread.sleep(2000);
            }
        }
    }

    /**
     * mysql优雅关闭spark streaming
     * @param appName 应用程序名称
     * @param javaStreamingContext JavaStreamingContext
     * @throws InterruptedException InterruptedException
     */
    public static void stopByMarkKeyMySql_bak(String appName, JavaStreamingContext javaStreamingContext) throws InterruptedException {
        //运行程序创建表
//        String createSql = "CREATE TABLE IF NOT EXISTS `STOPBYMARK` (" +
//                "  `APPNAME` varchar(255) NOT NULL," +
//                "  `FLAG` int(255) NOT NULL," +
//                "  `STOPTIME` bigint(255) NOT NULL," +
//                "  `UPTIME` varchar(255) NOT NULL" +
//                ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
//
//        MySqlUtil.getInstance().ExecuteSQL(createSql);

        String selectFlag = "SELECT * FROM `STOPBYMARK` WHERE APPNAME='" + appName+"'";
        String deleteFlag = "DELETE FROM `STOPBYMARK` WHERE APPNAME='" + appName+"'";
        Long stopTime = 60000L;//默认停止时间
        int intervalMills = 10 * 1000; // 每隔10秒扫描一次消息是否存在
        boolean isStop = false;
        boolean beginKill = false;//true表示开始停止程序了
        while (!isStop) {
            if(beginKill){
                return;
            }
            isStop = javaStreamingContext.awaitTerminationOrTimeout(intervalMills);
            //获取标识
            String flag = "";
            List<Map<String, Object>> maps = MySqlUtil.getInstance().QuerySQL(selectFlag);
            if(maps.size() > 0)
            {
                flag = maps.get(0).get("FLAG").toString();
                stopTime = Long.valueOf(maps.get(0).get("STOPTIME").toString());
            }

            if(flag == null|| StringUtils.isEmpty(flag))
            {
                Date date = new Date();
                String insertFlag = "INSERT INTO  `STOPBYMARK`  VALUES('"+appName+"',1,"+stopTime+",'"+fastDateFormat.format(date)+"')";
                MySqlUtil.getInstance().ExecuteSQL(insertFlag);
                flag = "1";
            }
            if (!isStop && flag.equals("0")) {//flag 为0表示需要停止程序
                //删除key
                beginKill = true;
                MySqlUtil.getInstance().ExecuteSQL(deleteFlag);
                System.out.println(stopTime + " millis sencond  after close "+appName+" app.....");
                Thread.sleep(stopTime);
                javaStreamingContext.stop(true, true);
            }
        }
    }

    //start------2019年9月2日优雅关闭修改(修改原来方法)
    public static Object lock = new Object();
    /**
     * mysql优雅关闭spark streaming
     * @param appName 应用程序名称
     * @param javaStreamingContext JavaStreamingContext
     * @throws InterruptedException InterruptedException
     */
    public static void stopByMarkKeyMySql(String appName, JavaStreamingContext javaStreamingContext) throws InterruptedException {
        //运行程序创建表
        String selectFlag = "SELECT * FROM `STOPBYMARK` WHERE APPNAME='" + appName+"'";
        String deleteFlag = "DELETE FROM `STOPBYMARK` WHERE APPNAME='" + appName+"'";
        Long stopTime = Long.valueOf (30 * 1000);//默认停止时间
        int intervalMills = 10 * 1000; // 每隔10秒扫描一次消息是否存在
        boolean isStop = false;
        boolean beginKill = false;//true表示开始停止程序了
        while (!isStop) {
            synchronized (lock)
            {
                isStop = javaStreamingContext.awaitTerminationOrTimeout(intervalMills);
                if(isStop)
                {
                    String upStatus = "update Task set `status`=10 where APPNAME='" + appName+"'";
                    List<String> list = new ArrayList<>();
                    list.add(upStatus);
                    list.add(deleteFlag);
                    MySqlUtil.getInstance().ExecuteSqlList(list);
                    System.out.println("程序 "+appName+" 已经停止stop！");
//                    System.exit(0);
                }

                if(beginKill){
                    return;
                }
                //获取标识
                String flag = "";
                List<Map<String, Object>> maps = MySqlUtil.getInstance().QuerySQL(selectFlag);
                if(maps.size() > 0)
                {
                    flag = maps.get(0).get("FLAG").toString();
                    stopTime = Long.valueOf(maps.get(0).get("STOPTIME").toString());
                }

                if (!isStop && flag.equals("0")) {//flag 为0表示需要停止程序
                    //删除key
                    beginKill = true;
                    System.out.println("stop by mark close "+appName+" app.....");
                    javaStreamingContext.stop(true, true);
                }

                if(StringUtils.isEmpty(flag) && !beginKill)
                {
                    Date date = new Date();
                    String insertFlag = "INSERT INTO  `STOPBYMARK`  VALUES('"+appName+"',1,"+stopTime+",'"+fastDateFormat.format(date)+"')";
                    MySqlUtil.getInstance().ExecuteSQL(insertFlag);
                }
                Thread.sleep(2000);
            }
        }
    }
    //end------2019年9月2日优雅关闭修改(修改原来方法)
    static FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");
    /**
     * oracle优雅关闭spark streaming
     * @param appName 应用程序名称
     * @param javaStreamingContext JavaStreamingContext
     * @throws InterruptedException InterruptedException
     */
    public static void stopByMarkKeyOracle(String appName, JavaStreamingContext javaStreamingContext) throws InterruptedException {
        //运行程序创建表
        String tableName = "STOPBYMARK";
        Boolean tableExists = OracleUtil.getInstance().tableExists(tableName);
        if(!tableExists)
        {
            String createSql = "CREATE TABLE "+tableName+" (APPNAME VARCHAR2(100) NOT NULL , FLAG INT NOT NULL ,STOPTIME VARCHAR2(100) NOT NULL , UPTIME VARCHAR2(100) NOT NULL , CONSTRAINT STOPBYMARK_PK PRIMARY KEY (APPNAME )ENABLE )";
            OracleUtil.getInstance().ExecuteSQL(createSql);
        }

        String selectFlag = "SELECT * FROM "+tableName+" WHERE APPNAME ='" + appName+"'";
        String deleteFlag = "DELETE FROM "+tableName+" WHERE  APPNAME='" + appName+"'";
        Long stopTime = 60000L;//默认停止时间
        int intervalMills = 10 * 1000; // 每隔10秒扫描一次消息是否存在
        boolean isStop = false;
        boolean beginKill = false;//true表示开始停止程序了
        while (!isStop) {
            if(beginKill){
                return;
            }
            isStop = javaStreamingContext.awaitTerminationOrTimeout(intervalMills);
            //获取标识
            String flag = "";
            List<Map<String, Object>> maps = OracleUtil.getInstance().QuerySQL(selectFlag);
            if(maps.size() > 0)
            {
                flag = maps.get(0).get("FLAG").toString();
                stopTime = Long.valueOf(maps.get(0).get("STOPTIME").toString());
            }

            if(flag == null|| StringUtils.isEmpty(flag))
            {
                Date date = new Date();
                String insertFlag = "INSERT INTO "+tableName+" VALUES('"+appName+"',1,"+stopTime+",'"+fastDateFormat.format(date)+"')";
                OracleUtil.getInstance().ExecuteSQL(insertFlag);
                flag = "1";
            }
            if (!isStop && flag.equals("0")) {//flag 为0表示需要停止程序
                //删除key
                beginKill = true;
                OracleUtil.getInstance().ExecuteSQL(deleteFlag);
                System.out.println(stopTime + " millis sencond  after close "+appName+" app.....");
                Thread.sleep(stopTime);
                javaStreamingContext.stop(true, true);
            }
        }
    }

    public static class BigDataStreamingListener implements StreamingListener {
        @Override
        public void onBatchCompleted(StreamingListenerBatchCompleted batchCompleted) {

            Iterator<Object> iterator = batchCompleted.productIterator();
            while (iterator.hasNext())
            {
                Object next = iterator.next();
                System.out.println(next);
            }

            BatchInfo batchInfo = batchCompleted.batchInfo();
            Long execTime =Long.valueOf(batchInfo.processingDelay().get().toString());
            Long schedulingTime =Long.valueOf( batchInfo.schedulingDelay().get().toString());
            System.out.println("exec time ="+ execTime + " , scheduling time =" + schedulingTime);
        }

        @Override
        public void onStreamingStarted(StreamingListenerStreamingStarted streamingStarted) {

        }

        @Override
        public void onReceiverStarted(StreamingListenerReceiverStarted receiverStarted) {

        }

        @Override
        public void onReceiverError(StreamingListenerReceiverError receiverError) {

        }

        @Override
        public void onReceiverStopped(StreamingListenerReceiverStopped receiverStopped) {

        }

        @Override
        public void onBatchSubmitted(StreamingListenerBatchSubmitted batchSubmitted) {
        }

        @Override
        public void onBatchStarted(StreamingListenerBatchStarted batchStarted) {

        }

        @Override
        public void onOutputOperationStarted(StreamingListenerOutputOperationStarted outputOperationStarted) {

        }

        @Override
        public void onOutputOperationCompleted(StreamingListenerOutputOperationCompleted outputOperationCompleted) {

        }
    }
}




