package bigdata.java.platform.beans.livy;

public class AppInfo {
    String driverLogUrl;
    String sparkUiUrl;
    public String getDriverLogUrl() {
        return driverLogUrl;
    }

    public void setDriverLogUrl(String driverLogUrl) {
        this.driverLogUrl = driverLogUrl;
    }

    public String getSparkUiUrl() {
        return sparkUiUrl;
    }

    public void setSparkUiUrl(String sparkUiUrl) {
        this.sparkUiUrl = sparkUiUrl;
    }

    @Override
    public String toString() {
        return "AppInfo{" +
                "driverLogUrl='" + driverLogUrl + '\'' +
                ", sparkUiUrl='" + sparkUiUrl + '\'' +
                '}';
    }
}
